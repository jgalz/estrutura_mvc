<?php
//Ajudador de Todos os controllers
class controller {

    //Vai carregar o view que queremos que carregue
    public function loadView($viewName, $viewData = array()) {
        //Transforma os indices do array em $variaveis
        extract($viewData);

        //Faz a chamada da view
        require 'views/' . $viewName . '.php';
    }

    //Carregar os menu/footer que toda pagina deve ter
    public function loadTemplate($viewName, $viewData = array()) {
        require 'views/template.php';
    }
    
    //Carregando os arquivos de html, dentro do Template
    public function loadViewInTemplate($viewName, $viewData = array()) {
        extract($viewData);
        require 'views/' . $viewName . '.php';
    }
}