<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/css/style.css">    
    <title>Estrutura_MVC</title>
</head>
<body>

    <h3>MENU</h3>
    <a href="<?php echo BASE_URL ?>galeria">Acessar Galeria</a> | <a href="<?php echo BASE_URL ?>">Voltar Inicio</a>
    <hr>
    <!--  Carregar os Views, arquivos de HTML -->
    <?php $this->loadViewInTemplate($viewName, $viewData) ?>
</body>
</html>