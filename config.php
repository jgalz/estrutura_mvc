<?php
require 'environment.php';

$config = array();

//Servidor Local
if (ENVIRONMENT == 'development') {
    define("BASE_URL", "http://localhost/estrutura_mvc/");
    $config['dbname'] = 'estrutura_mvc';
    $config['host'] = 'localhost';
    $config['dbuser'] = 'root';
    $config['dbpass'] = '';
} else {
    define("BASE_URL", "http://meusite.com.br/");
    //Servidor Hospedado
    $config['dbname'] = 'estrutura_mvc';
    $config['host'] = 'localhost';
    $config['dbuser'] = 'root';
    $config['dbpass'] = '';
}

global $db;
//Conexão Banco de Dados
try {
    $db = new PDO("mysql:dbname=" . $config['dbname'] . ";host=" . $config['host'], $config['dbuser'], $config['dbpass']);
    //echo "Banco conectado";
} catch (PDOException $ex) {
    //Retorna ERRO
    echo "ERRO: {$ex->getMessage()}";
    exit;
}